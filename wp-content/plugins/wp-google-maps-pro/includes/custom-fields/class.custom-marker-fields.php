<?php

namespace WPGMZA;

global $wpdb;
$WPGMZA_TABLE_NAME_MARKERS_HAS_CUSTOM_FIELDS = $wpdb->prefix . 'wpgmza_markers_has_custom_fields';

require_once(__DIR__ . '/class.custom-map-object-fields.php');

class CustomMarkerFields extends CustomMapObjectFields
{
	public function __construct($marker_id)
	{
		global $WPGMZA_TABLE_NAME_MARKERS_HAS_CUSTOM_FIELDS;
		
		$this->meta_table_name = $WPGMZA_TABLE_NAME_MARKERS_HAS_CUSTOM_FIELDS;
		
		CustomMapObjectFields::__construct($marker_id);
	}
	
	public static function getCustomFieldValues($map_id, $field_id)
	{
		global $wpdb;
		global $wpgmaps_tblname;
		global $WPGMZA_TABLE_NAME_MARKERS_HAS_CUSTOM_FIELDS;
		
		$qstr = "
			SELECT value
			FROM $WPGMZA_TABLE_NAME_MARKERS_HAS_CUSTOM_FIELDS
			WHERE object_id IN (
				SELECT id FROM $wpgmaps_tblname WHERE map_id = %d
			)
			AND field_id = %d
			GROUP BY value
		";
		
		$params = array($map_id, $field_id);
		
		$stmt = $wpdb->prepare($qstr, $params);
		
		return $wpdb->get_results($stmt);
	}
	
	// /**
	 // * Returns true if the named meta field is set
	 // * @return bool
	 // */
	// public function __isset($name)
	// {
		// return $this->__issetDelegate($name);
	// }
	
	// /**
	 // * Gets the named meta field from this objects cache
	 // * @return mixed
	 // */
	// public function __get($name)
	// {
		// return $this->__getDelegate($name);
	// }
	
	// /**
	 // * Sets the named meta field in this objects cache and the database
	 // * @return void
	 // */
	// public function __set($name, $value)
	// {
		// return __setDelegate($name, $value);
	// }
	
	// /**
	 // * Removes the named meta field from the cache and deletes it from the database
	 // * @return void
	 // */
	// public function __unset($name)
	// {
		// $this->__unsetDelegate();
	// }
}

// Hook into this filter to use your own subclass of CustomMarkerFields (for example, for custom layout)
add_filter('wpgmza_get_marker_custom_fields', 'WPGMZA\\get_marker_custom_fields');
function get_marker_custom_fields($marker_id)
{
	return new CustomMarkerFields($marker_id);
}
