<?php
/**
 * Template Name: Start page
 *
 * @package WordPress
 * @subpackage Brandson Theme
 * @since Brandson Theme 1.0
 */

get_header();?>


<!-- Intro Section -->
<div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php the_post_thumbnail_url()?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="mask rgba-stylish-light">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row pt-5 mt-3">
                <div class="col-md-12 mb-3">
                    <div class="intro-info-content text-center">
                        <h1 class="display-3 white-text mb-5 wow fadeInDown h1-responsive mx-auto" data-wow-delay="0.3s">
                            <?php the_field('rubrik');?>
                        </h1>
                        <p class="white-text mb-5 wow fadeInDown" data-wow-delay="0.3s">
                            <?php the_field('information');?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</header>

<div class="container">
    <div class="row d-flex justify-content-around align-content-center text-center mx-auto">

        <?php
        if ( have_rows('tjanster-repeater') ):

            while ( have_rows('tjanster-repeater') ) : the_row();
    ?>
        <?php
            $header = get_sub_field('tjanster-header');
            $text = get_sub_field('tjanster-text');
            $img = get_sub_field('tjanster-img');
            $link = get_sub_field('tjanster-lank');
    ?>



        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-10 col-12 mt-5 mb-5 p-2">

            <div class="card h-100">
                <img class="card-img-top" style="height: 15rem;" src="<?php echo $img; ?>" alt="<?php the_title(); ?>">
                <div class="card-body d-flex flex-column">

                    <h4 class="card-title mt-2 text-center">
                        <?php echo $header; ?>
                    </h4>
                    <p class="card-text text-center">
                        <?php echo $text; ?>
                    </p>

                    <a href="<?php the_permalink($link); ?>" class="btn btn-main mt-auto">
                        Gå till tjänst</a>

                </div>
            </div>
        </div>


        <?php

    
            endwhile;

            else :

            // no rows found

            endif;
            ?>

    </div>
</div>

<!-- Main Layout -->
<main>

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-12 mt-4 p-2">
                <?php
                if (have_posts() ) : while(have_posts() ) : the_post(); ?>
                <p class="text-black text-left">
                    <?php the_content(); ?>
                </p>
                <?php echo do_shortcode('[wpgmza id="1"]'); ?>
            </div>


            <div class="col-lg-4 col-md-4 col-sm-12 col-12 mb-4 p-2">
                <?php echo do_shortcode('[contact-form-7 id="133" title="Kontakt"]'); ?>
            </div>
        </div>
    </div>

    <?php endwhile; else: ?>
    <?php endif; ?>
</main>
<!-- Main Layout -->

<?php get_footer();?>