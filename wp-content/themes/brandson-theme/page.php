<?php 
/* Template name: regular pages 
/*
/*
*/

get_header();?>


<!-- Intro Section -->
<div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php the_post_thumbnail_url()?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;"
    alt="<?php the_title();?>">
    <div class="mask rgba-stylish-light">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row pt-5 mt-3">
                <div class="col-md-12 mb-3">
                    <div class="intro-info-content text-center">
                        <h1 class="display-3 white-text mb-5 h1-responsive wow fadeInDown mx-auto" data-wow-delay="0.3s">
                            <?php the_title()?>
                        </h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</header>
<!-- Main Navigation -->

<!-- Main Layout -->
<main>

    <div class="container mt-5 mb-5">

        <?php
                if (have_posts() ) : while(have_posts() ) : the_post(); ?>

        <?php the_content(); ?>


    </div>
    <?php endwhile; else: ?>
    <?php endif; ?>
</main>
<!-- Main Layout -->

<?php get_footer();?>