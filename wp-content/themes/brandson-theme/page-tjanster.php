<?php 
/* Template name: Tjänster
/*
/*
*/

get_header();?>


<!-- Intro Section -->
<div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php the_post_thumbnail_url()?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;"
    alt="<?php the_title(); ?>">
    <div class="mask rgba-stylish-light">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row pt-5 mt-3">
                <div class="col-md-12 mb-3">
                    <div class="intro-info-content text-center">
                        <h1 class="display-3 white-text mb-5 h1-responsive wow fadeInDown mx-auto" data-wow-delay="0.3s">
                            <?php the_title()?>
                        </h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</header>
<?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<ol class="breadcrumbs breadcrumb light-gray"><div class="container">','</div></ol>');
        }
    ?>



<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <?php the_content(); ?>
        </div>

<div class="col-lg-6 col-md-6 col-sm-6 col-12">
        <?php
        if ( have_rows('tjanster-repeater') ):

            while ( have_rows('tjanster-repeater') ) : the_row();
    ?>
        <?php
            $header = get_sub_field('header');
            $content = get_sub_field('content');
            $link = get_sub_field('link');
            
    ?>



        

            <div class="card mt-5 text-white bg-danger" style="width: 23rem; height:8rem;">
                <div class="card-body text-center d-flex align-items-center justify-content-center">
                    <h4 class="card-title">
                        <?php echo $header; ?>
                    </h4>

                </div>
            </div>

        
        


        <?php

    
            endwhile;

            else :

            // no rows found

            endif;
            ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                <?php echo do_shortcode('[contact-form-7 id="133" title="Kontakt"]'); ?>
            </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <p class="font-italic mt-5">
                <?php the_field('tjanster-slogan'); ?>
            </p>
        </div>
    </div>
</div>

<?php get_footer();?>