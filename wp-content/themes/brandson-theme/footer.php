    <!-- Footer -->
    <footer class="page-footer pt-4 mt-4 text-center text-md-left">

        <!-- Footer Links -->
        <div class="container">
            <div class="row d-flex">

                <!-- First column -->
                <div class="col-lg-8 col-12 col-sm-12 col-md-8">
                    <h5 class="text-uppercase font-weight-bold">Våra orter</h5>
                    <p>Vi finns på följande orter:
                        <a href="<?php echo get_the_permalink(37); ?>"> <?php the_field('Lulea', 'option'); ?></a>, 
                        <a href="<?php echo get_the_permalink(39); ?>"> <?php the_field('Skelleftea', 'option'); ?></a>, 
                        <a href="<?php echo get_the_permalink(43); ?>"> <?php the_field('Umea', 'option'); ?></a>,
                        <a href="<?php echo get_the_permalink(47); ?>"> <?php the_field('Ostersund', 'option'); ?></a>, 
                        <a href="<?php echo get_the_permalink(45); ?>"> <?php the_field('Ornskoldsvik', 'option'); ?></a>, 
                        <a href="<?php echo get_the_permalink(35); ?>"> <?php the_field('Harnosand', 'option'); ?></a>,
                        <a href="<?php echo get_the_permalink(41); ?>"> <?php the_field('Sundsvall', 'option'); ?></a>
                         . <br> Välkommen som kund!</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-12">
                <a href="tel:020-54 54 68" span class=" text-white py-1 d-flex justify-content-center"><i class="fa fa-phone mr-2 py-1" aria-hidden="true"></i>020-54 54 68</a></span>
                <a href="mailto:info@rentav.se" span class="text-white py-1 d-flex justify-content-center"><i class="fa fa-envelope-o mr-2 py-1" aria-hidden="true"></i>info@rentav.se</a></span>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-12">
                <a href="https://sv-se.facebook.com/RENTAVs/" span class="text-white py-1 d-flex justify-content-center"><i class="fa fa-facebook mr-2 py-1" aria-hidden="true"></i>info@rentav.se</a></span>
                <a href="https://www.instagram.com/rentavstadochmiljovard/" span class="text-white py-1 d-flex justify-content-center"><i class="fa fa-instagram mr-2 py-1" aria-hidden="true"></i>info@rentav.se</a></span>
                </div>

            </div>
        </div>
        <!-- Footer Links -->

		<!-- Copyright -->
		<?php wp_footer();?>

    </footer>
    <!-- Footer -->

    <!--  SCRIPTS  -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/mdb.min.js"></script>
    <script>
        new WOW().init();

        // MDB Lightbox Init
        $(function () {
            $("#mdb-lightbox-ui").load("<?php echo esc_url(get_template_directory_uri()); ?>/mdb-addons/mdb-lightbox-ui.html");
        });

    </script>
</body>
</html>
