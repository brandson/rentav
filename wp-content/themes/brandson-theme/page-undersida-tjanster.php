<?php 
/* Template name: SEO - Undersida stad
/*
/*
*/

get_header();?>


<!-- Intro Section -->
<div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->post_parent) ); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;"
    alt="<?php the_title(); ?>">
    <div class="mask rgba-stylish-light">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row pt-5 mt-3">
                <div class="col-md-12 mb-3">
                    <div class="intro-info-content text-center">
                        <h1 class="display-3 white-text mb-5 h1-responsive wow fadeInDown mx-auto" data-wow-delay="0.3s">
                            <?php the_title()?>
                        </h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</header>
<?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<ol class="breadcrumbs breadcrumb light-gray"><div class="container">','</div></ol>');
        }
    ?>
<div class="container mt-5 mb-5">
    <div class="row">



        <div class="col-lg-6 col-md-6 col-sm-6 mt-5 mb-5">




            <?php
                if (have_posts() ) : while(have_posts() ) : the_post(); ?>

            <?php the_content(); ?>



            <?php endwhile; else: ?>
            <?php endif; ?>


        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12 mt-5 mb-5">
            <?php echo do_shortcode('[contact-form-7 id="133" title="Kontakt"]'); ?>
            <?php
            if ( have_rows('kontaktuppgifter-seo') ):

            while ( have_rows('kontaktuppgifter-seo') ) : the_row();
        
                    $contactheader = get_sub_field('contact-header');
                    $contactperson = get_sub_field('contactperson');
                    $phonenumber = get_sub_field('phonenumber');
                    $email = get_sub_field('email');
                    $salesnumber = get_sub_field('phonenumber-sales');
                    $salesperson = get_sub_field('sales');
                    
                    
        ?>
            <div class="card mt-5 text-white bg-danger">
                <div class="card-body text-center d-block align-items-center justify-content-center">
                    <h4 class="card-title">
                        <?php echo $contactheader; ?>
                    </h4>

                    <h5 class="card-title h5-responsive">
                        <?php echo $contactperson; ?>
                    </h5>
                    <p class="card-title">
                        <a href="tel:<?php echo $phonenumber;?>">
                            <?php echo $phonenumber; ?></a>
                    </p>
                    <p class="card-title">
                        <?php echo $salesperson; ?>
                    </p>
                    <p class="card-title">
                        <a href="tel:<?php echo $salesnumber;?>">
                            <?php echo $salesnumber; ?></a>
                    </p>
                    <p class="card-title">
                        <a href="mailto:<?php echo $email;?>">
                            <?php echo $email; ?></a>
                    </p>

                </div>
            </div>
            <?php


            endwhile;

            else :

            // no rows found

            endif;
            ?>
        </div>



        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <?php echo do_shortcode('[wpgmza id="1"]'); ?>
        </div>


    </div>

</div>









<?php get_footer();?>