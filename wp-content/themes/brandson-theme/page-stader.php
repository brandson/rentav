<?php 
/* Template name: Städer
/*
/*
*/

get_header();?>


<!-- Intro Section -->
<div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php the_post_thumbnail_url()?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;"
    alt="<?php the_title(); ?>">
    <div class="mask rgba-stylish-light">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row pt-5 mt-3">
                <div class="col-md-12 mb-3">
                    <div class="intro-info-content text-center">
                        <h1 class="display-3 white-text mb-5 h1-responsive wow fadeInDown mx-auto" data-wow-delay="0.3s">
                            <?php the_title()?>
                        </h1>
                        <p class="text-uppercase white-text mb-5 mt-1 font-weight-bold wow fadeInDown" data-wow-delay="0.3s">
                            <?php the_field('text-header'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</header>
<?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<ol class="breadcrumbs breadcrumb light-gray"><div class="container">','</div></ol>');
        }
    ?>



<div class="container mt-5 mb-5">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12 mt-5 p-2 d-inline">
            <?php
                if ( have_rows('referenser-repeater') ):
                    while ( have_rows('referenser-repeater') ) : the_row();

                    $company = get_sub_field('company');
                    $person = get_sub_field('person');
                    $reference = get_sub_field('referens-text'); 
                    $icon = get_sub_field('icon');

                ?>
            <img class="img-fluid mt-2 mb-3" src="<?php echo $icon; ?>" />
            <p class="font-weight-bold">
                <?php echo $company; ?>
            </p>
            <p class="font-weight-normal">
                <?php echo $person; ?>
            </p>
            <p class="font-italic">
                <?php echo $reference; ?>
            </p>


            <?php


endwhile;

else :

// no rows found

endif;
?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12 p-2">
            <?php
            if ( have_rows('stader-repeater') ):

            while ( have_rows('stader-repeater') ) : the_row();
        
        
                    $link = get_sub_field('link');
                    $buttonheader = get_sub_field('button-header');
                    
        ?>






            <a href="<?php the_permalink($link); ?>">
                <div class="card mt-5 text-white bg-danger" style="width: 23rem; height:8rem;">
                    <div class="card-body text-center d-flex align-items-center justify-content-center">
                        <h2 class="card-title">
                            <?php echo $buttonheader; ?>
                        </h2>
            </a>
        </div>
    </div>



    <?php


            endwhile;

            else :

            // no rows found

            endif;
            ?>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-12 p-2">
    <?php echo do_shortcode('[contact-form-7 id="133" title="Kontakt"]'); ?>
    <?php
            if ( have_rows('kontaktuppgifter') ):

            while ( have_rows('kontaktuppgifter') ) : the_row();
        
                    $contactheader = get_sub_field('contact-header');
                    $contactperson = get_sub_field('contactperson');
                    $phonenumber = get_sub_field('phonenumber');
                    $email = get_sub_field('email');
                    $phonenumbersales = get_sub_field('phonenumber-sales');
                    $salesperson = get_sub_field('sales');
                    
                    
        ?>
    <div class="card mt-5 text-white bg-danger">
        <div class="card-body text-center d-block align-items-center justify-content-center">
            <h4 class="card-title">
                <?php echo $contactheader; ?>
            </h4>

            <h5 class="card-title h5-responsive">
                <?php echo $contactperson; ?>
            </h5>
            <p class="card-title">
                <a href="tel:<?php echo $phonenumber; ?>">
                    <?php echo $phonenumber; ?></a>
            </p>
            <p class="card-title">
                <?php echo $salesperson; ?>
            </p>
            <p class="card-title">
                <?php echo $phonenumbersales; ?>
            </p>
            <p class="card-title">
                <a href="mailto:<?php echo $email;?>">
                    <?php echo $email; ?></a>
            </p>

        </div>
    </div>
    <?php


            endwhile;

            else :

            // no rows found

            endif;
            ?>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-12 p-2 mt-4">
    <?php the_content(); ?>
    <?php echo do_shortcode('[wpgmza id="1"]'); ?>
</div>

</div>

</div>










<?php get_footer();?>